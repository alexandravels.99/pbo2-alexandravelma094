package ukm;
import java.util.Scanner;
public class Mahasiswa extends Penduduk implements Peserta {
    public Scanner sc = new Scanner(System.in);
    private String nim;
    public Mahasiswa() {
    }
    public Scanner getSc() {
        return sc;
    }
    public void setSc(Scanner sc) {
        this.sc = sc;
    }
    public String getNim() {
        return nim;
    }
    public void setNim(String nim) {
        this.nim = nim;
    }
    @Override
    public double hitungIuran() {
        long nim2 = Long.parseLong(nim); //convert from string to long
        return nim2 / 10000;
    }
    public void inputDataMhs() {
        System.out.println("Masukkan data diri Anda.");
        System.out.print("Nama\t\t: ");
        nama = sc.next();
        System.out.print("Tgl Lahir\t: ");
        tglLahir = sc.next();
        System.out.print("NIM\t\t: ");
        nim = sc.next();
    }
    @Override
    public String getJenisSertifikat() {
        return "Panitia";
    }
    @Override
    public String getFasilitas() {
        return "block note, alat tulis, laptop";
    }
    @Override
    public String getKonsumsi() {
        return "snack, makan siang, dan makan malam";
    }
}
