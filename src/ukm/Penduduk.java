package ukm;
abstract class Penduduk {
  protected String nama, tglLahir;

    public Penduduk() {
    }
    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getTglLahir() {
        return tglLahir;
    }
    public void setTglLahir(String tglLahir) {
        this.tglLahir = tglLahir;
    }
    abstract double hitungIuran();
  
  }
