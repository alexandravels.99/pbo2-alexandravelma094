package ukm;
import java.util.Scanner;
public class Masyarakat extends Penduduk implements Peserta {

    public Scanner sc = new Scanner(System.in);
    private String nomor;
    public Masyarakat() {
    }
    public Scanner getSc() {
        return sc;
    }
    public void setSc(Scanner sc) {
        this.sc = sc;
    }
    public String getNomor() {
        return nomor;
    }
    public void setNomor(String nomor) {
        this.nomor = nomor;
    }
    @Override
    public double hitungIuran() {
        String nomorr = nomor.substring(0, 3);
        return Long.parseLong(nomorr) * 100;
    }
    public void inputDataMsy() {
        System.out.println("Masukkan data diri Anda.");
        System.out.print("Nama\t\t: ");
        nama = sc.next();
        System.out.print("Tgl Lahir\t: ");
        tglLahir = sc.next();
        System.out.print("Nomor\t\t: ");
        nomor = sc.next();
    }
    @Override
    public String getJenisSertifikat() {
        return "Peserta";
    }
    @Override
    public String getFasilitas() {
        return "block note, alat tulis, dan modul pelatihan";
    }
    @Override
    public String getKonsumsi() {
        return "snack dan makan siang";
    }
}
