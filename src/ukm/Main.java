package ukm;
import java.util.Scanner;
public class Main {

    static Scanner sc = new Scanner(System.in);
    static Penduduk[] anggota;
    static UKM ukm = new UKM();
    static Mahasiswa ketua = new Mahasiswa();
    static Mahasiswa sekret = new Mahasiswa();
    static Mahasiswa mhs = new Mahasiswa();
    static Masyarakat msy = new Masyarakat();
    static int byk, pilih;
    static double total = 0;
    static void dataUKM() {
        String nama = "Dodo";
        ukm.setNamaUnit("Cetar Membahana");
        ketua.setNama("Dodo");
        sekret.setNama("Lisa");
        System.out.println("Nama UKM\t\t: " + ukm.getNamaUnit());
        System.out.println("Nama Ketua\t\t: " + ketua.getNama());
        System.out.println("Nama Sekretaris\t\t: " + sekret.getNama());
    }
    static void khususAnggota() {
        do {
            System.out.print("Masukkan Jumlah Anggota\t: ");
            byk = sc.nextInt();
            System.out.println();
        } while (byk <= 0);
        System.out.println("ANGGOTA UKM:");
        System.out.println("1. Mahasiswa");
        System.out.println("2. Masyarakat Sekitar");
        System.out.println("");
        anggota = new Penduduk[byk];
        for (int i = 0; i < byk; i++) {
            System.out.println("Masukan Data Anggota ke-" + (i + 1));
            do {
                System.out.print("Asal Anggota\t: ");
                pilih = sc.nextInt();
            } while (pilih <= 0 && pilih > 2);
            if (pilih == 1) {
                System.out.println("Anggota UKM adalah Mahasiswa.");
                System.out.println("");
                mhs.inputDataMhs();
                anggota[i] = mhs;
            } else if (pilih == 2) {
                System.out.println("Anggota UKM adalah Masyarakat.");
                System.out.println("");
                msy.inputDataMsy();
                anggota[i] = msy;
            }
            System.out.println();
        }
    }
    static void cetakData() {
            ukm.setAnggota(anggota);
        for (int i = 0; i < byk; i++) {
            System.out.printf("%-7s", (i + 3) + ".");
            if (anggota[i] instanceof Mahasiswa) {
                mhs = (Mahasiswa) anggota[i];
                System.out.printf("%-10s", mhs.getNama());
                System.out.printf("%-8s", mhs.hitungIuran());
                System.out.printf("%-13s", mhs.getNim());
                System.out.printf("%-14s", mhs.getJenisSertifikat());
                System.out.printf("%-34s", mhs.getFasilitas());
                System.out.printf("%-39s", mhs.getKonsumsi());
                System.out.println("");
            } else if (anggota[i] instanceof Masyarakat) {
                msy = (Masyarakat) anggota[i];
                System.out.printf("%-10s", msy.getNama());
                System.out.printf("%-8s" , msy.hitungIuran());
                System.out.printf("%-13s", msy.getNomor());
                System.out.printf("%-14s", msy.getJenisSertifikat());
                System.out.printf("%-34s", msy.getFasilitas());
                System.out.printf("%-39s", msy.getKonsumsi());
                System.out.println("");
            }
            total = total  + anggota[i].hitungIuran();
        }
        
    }
    public static void main(String[] args) {
        khususAnggota();
        dataUKM();
        System.out.println("DATA ANGGOTA UKM");
        System.out.println("============================================================================"
                + "=============================================");
        System.out.printf("%-7s", "No.");
        System.out.printf("%-10s", "Nama");
        System.out.printf("%-8s", "Iuran");
        System.out.printf("%-13s", "NIM/NIK");
        System.out.printf("%-14s", "Sertifikat");
        System.out.printf("%-34s", "Fasilitas");
        System.out.printf("%-39s", "Konsumsi");
        System.out.println("");
        System.out.println("============================================================================"
                + "=============================================");
        for (int j = 0; j < 2; j++) {
            System.out.printf("%-7s", (j + 1) + ".");
            if (j == 0) {
                ketua.setNim("185314094");
                System.out.printf("%-10s", ketua.getNama());
                System.out.printf("%-8s", "-");
                System.out.printf("%-13s", ketua.getNim());
                System.out.printf("%-14s", ketua.getJenisSertifikat());
                System.out.printf("%-34s", ketua.getFasilitas());
                System.out.printf("%-39s", ketua.getKonsumsi());
                System.out.println("");
            }
            else if (j == 1){
                sekret.setNim("185314112");
                System.out.printf("%-10s", sekret.getNama());
                System.out.printf("%-8s", "-");
                System.out.printf("%-13s", sekret.getNim());
                System.out.printf("%-14s", sekret.getJenisSertifikat());
                System.out.printf("%-34s", sekret.getFasilitas());
                System.out.printf("%-39s", sekret.getKonsumsi());
                System.out.println("");
            }
        }
        cetakData();
        System.out.println("============================================================================"
                + "=============================================");
        System.out.println("Total Iuran Anggota : Rp " +total);
        System.out.println();
    }
}
