package ukm;
public class UKM {
private String namaUnit;
private Mahasiswa ketua, sekretrs;
private Penduduk[] anggota;

    public UKM() {
    }
    public UKM(Mahasiswa ketua, Mahasiswa sekretrs) {
        this.ketua = ketua;
        this.sekretrs = sekretrs;
    }
    public String getNamaUnit() {
        return namaUnit;
    }
    public void setNamaUnit(String namaUnit) {
        this.namaUnit = namaUnit;
    }
    public Mahasiswa getKetua() {
        return ketua;
    }
    public void setKetua(Mahasiswa ketua) {
        this.ketua = ketua;
    }
    public Mahasiswa getSekretrs() {
        return sekretrs;
    }
    public void setSekretrs(Mahasiswa sekretrs) {
        this.sekretrs = sekretrs;
    }
    public Penduduk[] getAnggota() {
        return anggota;
    }
    public void setAnggota(Penduduk[] anggota) {
        this.anggota = anggota;
    }
    


}
